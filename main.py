import traceback
import time
import fastapi
import uvicorn
import datetime
import sqlite3
import threading
import random
import logging
import string


#####################
app = fastapi.FastAPI()

conn_users=sqlite3.connect('database_users.db',check_same_thread=False)
conn_tokens=sqlite3.connect('database_tokens.db',check_same_thread=False)

cursor_users=conn_users.cursor()
cursor_tokens=conn_tokens.cursor()

cursor_users.execute(f'CREATE TABLE IF NOT EXISTS users('
               f'id STRING NOT NULL,'
               f'login STRING NOT NULL,'
               f'password STRING NOT NULL,'
               f'payment DOUBLE NOT NULL'
               f')')
conn_users.commit()

cursor_tokens.execute(f'CREATE TABLE IF NOT EXISTS tokens('
               f'id STRING NOT NULL,'
               f'token STRING NOT NULL,'
               f'date DATETIME NOT NULL'
               f')')
conn_tokens.commit()

tokens_timeout=300
secure_code='123456'
lock=threading.Lock()
#####################


def generate_token(length:int):
    characters = string.ascii_letters + string.digits
    token = ''.join(random.choice(characters) for _ in range(length))
    return token


def check_token():
    while True:
        try:
            with lock:
                cursor_tokens.execute(f"DELETE FROM tokens WHERE datetime(date,'+{tokens_timeout} second')<=DATETIME('now')")
                conn_tokens.commit()
        except:
            logging.error(traceback.format_exc())
        time.sleep(5)


threading.Thread(target=check_token).start()


@app.post("/create_user/")
def create_user_handler(login: str, password: str,payment: float,code: str):
    try:
        if not code:
            return {'error': 'code is required'}
        if not login:
            return {'error': 'login is required'}
        if not password:
            return {'error': 'password is required'}
        if not payment:
            return {'error': 'payment is required'}

        if code!=secure_code:
            return {'error': 'code is incorrect'}

        cursor_users.execute("SELECT * FROM users WHERE login=? and password=?", (login, password))
        data = cursor_users.fetchone()

        if not data:
            while True:
                id = generate_token(5)
                cursor_users.execute(f"SELECT * FROM users WHERE id='{id}'")
                if not cursor_users.fetchone():
                    break
            cursor_users.execute(f"INSERT INTO users VALUES(?,?,?,?)",(id,login,password,payment))
            conn_users.commit()
            return {'error': '','id':id,'login':login,'password':password,'payment':payment}
        else:
            return {'error': 'user is already exists'}
    except:
        logging.error(traceback.format_exc())
        raise fastapi.HTTPException(status_code=500, detail="Internal Server Error")



@app.get("/login/")
def login_user_handler(login: str, password: str):
    try:
        if not login:
            return {'error':'login is required'}
        if not password:
            return {'error':'password is required'}


        cursor_users.execute("SELECT * FROM users WHERE login=? and password=?", (login, password))
        data=cursor_users.fetchone()

        if data:
            while True:
                token = generate_token(13)
                with lock:
                    cursor_tokens.execute(f"SELECT * FROM tokens WHERE token='{token}'")
                    if not cursor_tokens.fetchone():
                        break
            with lock:
                cursor_tokens.execute(f"INSERT INTO tokens VALUES('{data[0]}','{token}',datetime('now'))")
                conn_tokens.commit()

            return {'token':token,'error':'','timeout':tokens_timeout}
        else:
            return {'error':'incorrect login or password'}

    except:
        logging.error(traceback.format_exc())
        raise fastapi.HTTPException(status_code=500, detail="Internal Server Error")


@app.get("/payment_info/")
def get_payment_info_handler(token: str):
    try:
        if not token:
            return {'error': 'token is required'}

        with lock:
            cursor_tokens.execute("SELECT * FROM tokens WHERE token=?", (token,))
            data = cursor_tokens.fetchone()

        if data:
            cursor_users.execute("SELECT payment FROM users WHERE id=?",(data[0],))
            data_user=cursor_users.fetchone()
            if data_user:
                return {'error':'','payment':data_user[0]}
            else:
                return {'error': 'cant get user information'}
        else:
            return {'error': 'incorrect token'}

    except:
        logging.error(traceback.format_exc())
        raise fastapi.HTTPException(status_code=500, detail="Internal Server Error")

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)